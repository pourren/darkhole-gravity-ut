#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 16:15:14 2022

@author: pourren
"""

import argparse
from datetime import datetime
import time
import os
import ccs
import vlti
import numpy as np
from astropy.io import fits

# =============================================================================
# LAUNCH ON THE AT
# =============================================================================

if __name__ == '__main__':
    """add new SCAR15 offset in atcs"""
    parser = argparse.ArgumentParser(description="Update the modal offset")
    parser.add_argument('tel', type=int, choices=range(5), help="Telescope index")
    parser.add_argument('amp' , type=float , help="amplitude in µm rms coma and trefoil of the SCAR mode")
    parser.add_argument('angle' , type=float , help="angle in deg with respect to ???")
    args = parser.parse_args()
    
    ccs.CcsInit(name='add_to_ref_modal.py')
    ang_sky = args.angle * (2*np.pi/360) -np.pi
    delta_offset = np.array([0,0,0,0,0,0,args.amp,0,args.amp,0,0,0,0,0])
    # Send to the telescopes
    if (args.tel>=1) and (args.tel<=4):
        # Create the offset with the angle
        ang_chop = ccs.DbRead("@wat{0}tcs:Appl_data:TCS:nmupd:data.northAngle".format(args.tel))
        ang_tot = ang_chop + ang_sky
        rotmat=np.array([[np.cos(ang_tot),-np.sin(ang_tot),0,0],[np.sin(ang_tot),np.cos(ang_tot),0,0]
                          ,[0,0,np.cos(3*ang_tot),-np.sin(3*ang_tot)],[0,0,np.sin(3*ang_tot),np.cos(3*ang_tot)]])
        delta_offset[5:9]=rotmat.dot(delta_offset[5:9])
        hdul=fits.open('default_modal_offset/original_Acq.DET1.MODAL_OFFSET_{0}.fits'.format(args.tel))
        modal_offset=hdul[0].data[0]+delta_offset
        # SEND THE NEW ONE
        watNtcs = vlti.ssh("atcs@wat{0}tcs".format(args.tel))
        watNtcs.send("wat{0}nao".format(args.tel), "spaccsServer", "EXEC", "\" Acq.addModesToRef 1 \\\""+(" ".join(str(e) for e in modal_offset) +"\\\"\""),verbose=True)
    else:
        for tel in range(4):
            ang_chop = ccs.DbRead("@wat{0}tcs:Appl_data:TCS:nmupd:data.northAngle".format(tel+1))
            ang_tot = ang_chop + ang_sky
            rotmat=np.array([[np.cos(ang_tot),-np.sin(ang_tot),0,0],[np.sin(ang_tot),np.cos(ang_tot),0,0]
                              ,[0,0,np.cos(3*ang_tot),-np.sin(3*ang_tot)],[0,0,np.sin(3*ang_tot),np.cos(3*ang_tot)]])
            delta_offset[5:9]=rotmat.dot(delta_offset[5:9])
            hdul=fits.open('default_modal_offset/original_Acq.DET1.MODAL_OFFSET_{0}.fits'.format(tel+1))
            modal_offset=hdul[0].data[0]+delta_offset
            # SEND THE NEW ONE
            watNtcs = vlti.ssh("atcs@wat{0}tcs".format(tel+1))
            watNtcs.send("wat{0}nao".format(tel+1), "spaccsServer", "EXEC", "\" Acq.addModesToRef 1 \\\""+(" ".join(str(e) for e in modal_offset) +"\\\"\""),verbose=True)
    time.sleep(2)
